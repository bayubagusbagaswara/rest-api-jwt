# REST API with JWT

# Fitur

- Register User
- Login User
- Create Category
- Get All Categories
- Get Category By ID
- Update Category
- Create Transaction
- Get All Transactions
- Get Transaction By ID
- Update Transaction
- Delete Transaction
- Delete Category With All Transactions

# Alur Kerja

## User harus register dulu

- Method : `POST`
- URL : `/api/users/register`

- Request Body

```json
{
  "firstName": "string",
  "lastName": "string",
  "email": "string",
  "password": "string"
}
```

- Response Body

```json
{
  "token": "string"
}
```
## User akan melakukan login,
- Method : `POST`
- URL : `/api/users/login`

- Request body

```json
{
  "email": "string",
  "password": "string"
}
```
- Response body

```json
{
  "token": "string"
}
```

## Create Category

- Method : `POST`
- URL : `/api/categories`
- Headers : `Authorization : Bearer <token>`

- Request body

```json
{
  "title": "string",
  "description": "string"
}
```

- Response Error Forbidden

```json
{
  "status": "403",
  "error": "Forbidden",
  "message": "Authorization token must be provided"
}
```

- Response Success

```json
{
  "categoryId": "1",
  "userId": "1",
  "title": "string",
  "description": "string",
  "totalExpense": "number"
}
```

## Get All Categories
- Method : `GET`
- URL : `/api/categories`
- Headers : `Authorization : Bearer <token>`

- Response Body 

```json
[
  {
    "categoryId": "1",
    "userId": "1",
    "title": "string",
    "description": "string",
    "totalExpense": "number"
  },
  {
    "categoryId": "2",
    "userId": "1",
    "title": "string",
    "description": "string",
    "totalExpense": "number"
  }
]
```

## Get Category By ID
- Method : `GET`
- URL : `/api/categories/{categoryId}`
- Headers : `Authorization : Bearer <token>`

- Response body

```json
{
  "categoryId": "1",
  "userId": "1",
  "title": "string",
  "description": "string",
  "totalExpense": "number"
}
```

## Update Category 
- Method : `PUT`
- URL : `/api/categories/{categoryId}`
- Headers : `Authorization : Bearer <token>`

- Request body

```json
{
  "title": "string",
  "description": "string"
}
```

- Response
```json
{
  "success": "boolean"
}
```

## Delete Transaction
- Method : `DELETE`
- URL : `/api/categories/{categoryId}/transactions/{totalTransaksi}`
- Headers : `Authorization : Bearer <token>`
- Response body

```json
{
  "success": "boolean"
}
```
