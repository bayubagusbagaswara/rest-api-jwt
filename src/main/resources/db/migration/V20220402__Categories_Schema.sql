create table et_categories(
    category_id integer primary key not null,
    user_id integer not null,
    title varchar(20) not null,
    description varchar(50) not null
);

alter table et_categories
    add constraint cat_users_fk foreign key (user_id) references et_users(user_id);