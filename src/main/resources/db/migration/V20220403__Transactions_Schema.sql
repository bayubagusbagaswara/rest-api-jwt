create table et_transactions(
    transaction_id integer primary key not null,
    category_id integer not null,
    user_id integer not null,
    amount numeric(10,2) not null,
    note varchar(50) not null,
    transaction_data bigint not null
);

alter table et_transactions
    add constraint trans_cat_fk foreign key (category_id) references et_categories(category_id);

alter table et_transactions
    add constraint trans_user_fk foreign key (user_id) references et_users(user_id);