package com.api.jwt.services;

import com.api.jwt.exceptions.EtAuthException;
import com.api.jwt.domain.User;

public interface UserService {

    User validateUser(String email, String password) throws EtAuthException;

    User  registerUser(String firstName, String lastName, String email, String password) throws EtAuthException;


}
