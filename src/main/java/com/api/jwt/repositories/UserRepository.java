package com.api.jwt.repositories;

import com.api.jwt.exceptions.EtAuthException;
import com.api.jwt.domain.User;

public interface UserRepository {

    Integer create(String firstName, String lastName, String email, String password) throws EtAuthException;

    User findByEmailAndPassword(String email, String password) throws EtAuthException;

    Integer getCountByEmail(String email);

    User findById(Integer userId);
}
